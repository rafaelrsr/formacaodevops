# FormacaoDevOps

Após participar da Imersão DevOps Cloud Week pela Cloud Treinamentos, iniciei a Formação DevOps com finalidade de aprender e trabalhar com as diversas ferramentas dessa cultura incrível. Segue meu portifólio das aulas e seus respectivos links até aqui.

## Docker:
https://gitlab.com/rafaelrsr/formacaodevops/-/tree/main/docker

https://gitlab.com/rafaelrsr/formacaodevops/-/tree/main/docker-compose

## Jenkins:
https://gitlab.com/rafaelrsr/formacaodevops/-/tree/main/Jenkins

## Desafio Jenkins:
https://gitlab.com/rafaelrsr/desafiojenkins

## Zabbix
https://gitlab.com/rafaelrsr/formacaodevops/-/tree/main/zabbix

## Grafana
https://gitlab.com/rafaelrsr/formacaodevops/-/tree/main/grafana

## Nexus
https://gitlab.com/rafaelrsr/formacaodevops/-/tree/main/modulo-nexus